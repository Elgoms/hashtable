#pragma once
#include <memory>
#include <iostream>
#include <variant>
#include <utility>
#include "Hash.h"

namespace
{
	template<typename K, typename V>
	struct Node
	{
		K key;
		V value;
	};

	template<typename K>
	class Hasher
	{
		static_assert(!std::is_class_v<K> && !std::is_pointer_v<K>);
	public:
		static size_t Hash(K key)
		{
			auto bytes = reinterpret_cast<void*>(&key);
			return HashFunctions::FNV1a(bytes, sizeof(K));
		}
	};

	template<>
	class Hasher<std::string>
	{
	public:
		static size_t Hash(const std::string& key)
		{
			auto bytes = key.data();

			return HashFunctions::FNV1a(bytes, sizeof(key));
		}
	};
}

namespace GC_HashTable
{
	template<typename K, typename V, typename H = Hasher<K>>
	class HashTable : H
	{
	public:

		HashTable(const K& _empty, const K& _sentinel) 
		{
			size = 0; 
			capacity = 16; 
			nodes = std::make_unique<Node<K, V>[]>(capacity);

			empty = _empty;
			sentinel = _sentinel;

			for (int i = 0; i < capacity; i++)
			{
				nodes[i].key = empty;
			}
		};

		HashTable(K&& _empty, K&& _sentinel)
		{
			size = 0;
			capacity = 16;
			nodes = std::make_unique<Node<K, V>[]>(capacity);

			empty = _empty;
			sentinel = _sentinel;

			for (int i = 0; i < capacity; i++)
			{
				nodes[i].key = empty;
			}
		};

		~HashTable() = default;

		V* operator [] (const K& key) { return FindElement(key); };
		V* operator [] (K&& key) { return FindElement(key); };

		/// <summary>
		/// Tries to get the element at the given key
		/// </summary>
		/// <param name="key">rvalue reference</param>
		/// <returns>returns a reference to the element or a nullptr if it does not exist.</returns>
		V& GetElementByKey(K&& key)
		{
			return FindElement(key);
		}

		/// <summary>
		/// Tries to get the element at the given key
		/// </summary>
		/// <param name="key">rvalue reference</param>
		/// <returns>returns a reference to the element or a nullptr if it does not exist.</returns>
		V& GetElementByKey(const K& key)
		{
			return FindElement(key);
		}

		size_t Size() { return size; }


		/// <summary>
		/// Clears the interal pointer and resets the array
		/// </summary>
		void Clear() 
		{ 
			nodes.reset(); 
			size = 0;
			capacity = 16;
			nodes = std::make_unique<Node<K, V>[]>(capacity);
		}
		
		/// <summary>
		/// Insert an element with the specified key and returns a pointer to it if the operation was successful.
		/// </summary>
		/// <param name="key">rvalue reference</param>
		/// <param name="value">const lvalue reference</param>
		/// <returns>returns a pointer to the inserted element or return nullptr if it already exists.</returns>
		Node<K, V>* Insert(K&& key, const V& value)
		{
			if (size + sentinelCount >= capacity >> 1) //load factor 0.5
			{
				ExpandTable();
			}

			auto rst = LinearProbing(value, key, CalculateIndex(key));
			size = rst != nullptr ? size+1 : size;

			return rst;
		};

		/// <summary>
		/// Insert an element with the specified key and returns a pointer to it if the operation was successful.
		/// </summary>
		/// <param name="key">rvalue reference</param>
		/// <param name="value">rvalue reference</param>
		/// <returns>returns a pointer to the inserted element or return nullptr if it already exists.</returns>
		Node<K, V>* Insert(K&& key, V&& value)
		{
			if (size + sentinelCount >= capacity >> 1) //load factor 0.5
			{
				ExpandTable();
			}

			auto rst = LinearProbing(value, key, CalculateIndex(key));

			size = rst != nullptr ? size+1 : size;

			return rst;
		};

		/// <summary>
		/// Tries to find the specified element
		/// </summary>
		/// <param name="key">const lvalue reference</param>
		/// <returns>A pointer to the element if it exists or a nullptr if it doesn't.</returns>
		V& FindElement(const K& key)
		{
			 return LinearProbing(CalculateIndex(key), key)->value;
		}

		/// <summary>
		/// Tries to find the specified element
		/// </summary>
		/// <param name="key">rvalue reference</param>
		/// <returns>A pointer to the element if it exists or a nullptr if it doesn't.</returns>
		V& FindElement(K&& key)
		{
			return LinearProbing(CalculateIndex(key), key)->value;
		}

		/// <summary>
		/// Removes an element from the table
		/// </summary>
		/// <param name="key">const lvalue reference</param>
		/// <returns>returns true if the element was removed or false if it wasn't (it did not exist).</returns>
		bool RemoveElement(const K& key)
		{
			auto rst = LinearProbing(CalculateIndex(key), key);

			if (rst != nullptr)
			{
				rst->value.~V();
				rst->key = sentinel;
				sentinelCount++;
				size--;
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Removes an element from the table
		/// </summary>
		/// <param name="key">rvalue reference</param>
		/// <returns>returns true if the element was removed or false if it wasn't (it did not exist).</returns>
		bool RemoveElement(K&& key)
		{
			auto rst = LinearProbing(CalculateIndex(key), key);

			if (rst != nullptr)
			{
				rst->value.~V();
				rst->key = sentinel;
				sentinelCount++;
				size--;
				return true;
			}
			else
			{
				return false;
			}
		}

	private:
		K sentinel, empty;
		H hasher;
		std::unique_ptr<Node<K, V>[]> nodes;
		size_t size, capacity, sentinelCount;	

		/// <summary>
		/// Calculates and return the index corresponding to the specified key
		/// </summary>
		size_t CalculateIndex(const K& key)
		{
			size_t idx = hasher.Hash(key);
			idx &= (capacity - 1); //capacity is a power of two, better than using modulo %

			return idx;
		}

		/// <summary>
		/// Expands the table using a buffer filled by moving the table and recalculating the new indexes to copy them back
		/// </summary>
		void ExpandTable()
		{
			size_t lastCap = capacity;
			capacity <<= 1; //capacity * 2^1 --> capacity * 2

			std::unique_ptr<Node<K, V>[]> buff = std::move(nodes);
			nodes = std::make_unique<Node<K, V>[]>(capacity);

			//std::move(buff.get(), std::next(buff.get(), size), nodes.get());

			for (int i = 0; i < capacity; i++)
			{
				nodes[i].key = empty;
			}

			for (int i = 0; i < lastCap; i++)
			{
				if (buff[i].key != empty && buff[i].key != sentinel)
				{
					LinearProbing(buff[i].value, buff[i].key, CalculateIndex(buff[i].key));
				}
			}

			sentinelCount = 0;
		}

		/// <summary>
		/// Linear probing for insert
		/// </summary>
		/// <param name="value">const lvalue reference</param>
		/// <param name="key">const lvalue reference</param>
		/// <param name="index">const lvalue reference</param>
		/// <returns>A pointer to the node containing the inserted data or a nullptr if the key already existed</returns>
		Node<K, V>* LinearProbing(const V& value, const K& key, const size_t& index)
		{
			size_t internalCount = 0;
			size_t maxProbingIters = size / 2;
			int sentinelIndex = -1;

			for (size_t i = index; i < capacity; i++, internalCount++)
			{
				if (nodes[i].key == key)
				{
					return nullptr;
				}

				if (nodes[i].key == sentinel && sentinelIndex == -1)
				{
					sentinelIndex = i;
				}

				if (nodes[i].key == empty)
				{											  
					nodes[i].key = key; //all nodes are constructed (unique_ptr constructs them)
					nodes[i].value = value;
					return &nodes[i];
				}
			}

			for (size_t i = 0; i < index; i++) //check the beginning of the table
			{
				if (nodes[i].key == empty)
				{
					nodes[i].key = key; //all nodes are constructed (unique_ptr constructs them)
					nodes[i].value = value;
					return &nodes[i];
				}
			}

			nodes[sentinelIndex].key = key; //all nodes are constructed (unique_ptr constructs them)
			nodes[sentinelIndex].value = value;
			sentinelCount--;
			return &nodes[sentinelIndex];

			//if (internalCount > maxProbingIters)
			//{
			//	ExpandTable();
			//}
		}

		/// <summary>
		/// Linear probing for the find function
		/// </summary>
		Node<K, V>* LinearProbing(const size_t& index, const K& key)
		{
			for (size_t i = index; i < capacity; i++)
			{
				if (key == nodes[i].key)
				{
					return &nodes[i];
				}
			}

			for (size_t i = 0; i < index; i++) //check the beginning of the table
			{
				if (key == nodes[i].key)
				{
					return &nodes[i];
				}
			}

			return nullptr;
		}

	};
}


