#pragma once
#include <cstdint>

namespace HashFunctions
{
	constexpr uint32_t FNV1aImp(uint8_t byte, uint32_t state)
	{
		state ^= byte;
		state *= 0x01000193;
		return state;
	}

	constexpr uint64_t FNV1aImp(uint8_t byte, uint64_t state)
	{
		state ^= byte;
		state *= 0x00000100000001B3;
		return state;
	}

	inline uint32_t FNV1a(const void* ptr, uint32_t size)
	{
		uint32_t s = 0x811c9dc5;
		for (short i = 0; i < size; i++)
		{
			s = FNV1aImp((reinterpret_cast<const uint8_t*>(ptr))[i], s);
		}

		return s;
	}

	inline uint64_t FNV1a(const void* ptr, uint64_t size)
	{
		uint64_t s = 0xcbf29ce484222325;
		for (short i = 0; i < size; i++)
		{
			s = FNV1aImp((reinterpret_cast<const uint8_t*>(ptr))[i], s);
		}

		return s;
	}
}